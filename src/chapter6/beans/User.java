package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	
	
	private int id;
	private String account;
	private String name;
	private String email;
	private String password;
	private String description;
	private Date createDate;
	private Date updateDate;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getAccount() {
		return account;
	}
	
	public void setAccount(String account) {
		this.account = account;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public  String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date  getCreateDate() {
		return createDate;
	}
	
	public void setCreateData(Date createDate) {
		this.createDate = createDate;
	}
	
	public Date getUpdateDate() {
		return updateDate;
	}
	
	public void setUPdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}
